//
//  TablitsaCont.h
//  laba3semestr3
//
//  Created by Николай on 01.12.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//

#ifndef __laba3semestr3__TablitsaCont__
#define __laba3semestr3__TablitsaCont__

#include <stdio.h>
#include "myvector.h"
#include "Player.h"

struct Unit {
    Player* player;
    Unit():player(NULL){}
    Unit (Player* x):player(x){}
};
class TablitsaCont
{
private:
    mvector<Unit> units;
public:
    /**Добавить игрока
     @param указатель на игрока*/
    int add(Player*);
    /**Удалить игрока по фамилии */
    int del();
    /**Показать информацию об игроке по фамилии
     @param фамилия*/
    void showPlayerInfo(string);
    /**Получить дату, противника и состав команды */
    void getplinfo();
    /**Получить штрафное время игроков за игру
     @return время*/
    int getShTime() ;
    /**Очистить таблицу */
    void clearTabl();
    /**получить количество пропущенных мячей
     @return кол-во мячей*/
    int Turnir1_propush();
    /**получить количество забитых мячей
     @return кол-во мячей*/
    int Turnir2_zabit();
    /**Вывод таблицы */
    friend std::ostream &operator<<(std::ostream&, TablitsaCont&);
};
#endif /* defined(__laba3semestr3__TablitsaCont__) */
