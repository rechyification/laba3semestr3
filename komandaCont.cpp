//
//  komandaCont.cpp
//  laba3semestr3
//
//  Created by Николай on 01.12.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//

#include "komandaCont.h"
#include <iostream>
void komandaCont::Turnir(){
    mvector<Date>::iterator pp;
    for (auto pp = tableSet.begin(); pp != tableSet.end(); ++pp)
    {
        int p,z = 0;
        p = (*pp).tab->Turnir1();
        z = (*pp).tab->Turnir2();
        cout<<"Дата: "<<(*pp).day<<".";
        cout<<(*pp).month<<".";
        cout<<(*pp).year<<endl;
        cout<<"Противник: "<<(*pp).versus<<endl;
        cout<<"Финальный счет ";
        cout<<"Пропущено "<<p;
        cout<<" - "<<z<<" Забито"<<endl;
    }
    
}
int komandaCont::addTable()
{
    string vs;
    int d,m,y = 0;
    cout<<"Vvedite date ";
    cin>>d>>m>>y;
    cout<<"Vvedite versus ";
    cin>>vs;
    Tablitsa* x = new Tablitsa;
    Date* temp = new Date(d,m,y,x,vs);
    tableSet.push_back(*temp);
    //delete temp;
    return 1;
}
void komandaCont::datavspl(){
    mvector<Date>::iterator p;
    for (auto p = tableSet.begin(); p != tableSet.end(); ++p)
    {
        cout<<"Дата: "<<(*p).day<<".";
        cout<<(*p).month<<".";
        cout<<(*p).year<<endl;
        cout<<"Противник: "<<(*p).versus<<endl<<"Состав команды:"<<endl;
        (*p).tab->getplinfo();
        cout<<endl;
    }
    cout<<endl;
}
void komandaCont::finddate(){
    cout<<"Введите дату (день, месяц, год)";
    int d,m,y=0;
    cin>>d>>m>>y;
    if (tableSet.size1()){
        mvector<Date>::iterator p;
        for (auto p = tableSet.begin(); p != tableSet.end(); ++p)
        {
            if(d==(*p).day && m==(*p).month && y==(*p).year){
                cout<<"Versus: "<<(*p).versus<<endl;
                (*p).tab->show();
            }
        }
    }
}
int komandaCont::deldate()
{
    cout<<"Введите дату"<<endl;
    int d,m,y = 0;
    cin>>d>>m>>y;
    mvector<Date>::iterator p;
    for (auto p = tableSet.begin(); p != tableSet.end(); ++p)
    {
        if (((*p).day == d) && ((*p).month == m) && ((*p).year == y))
        {
            tableSet.erase(p);
            return 0;
        }
    }
    return 1;
}
int komandaCont::addPlayer()
{
    int q,pos;
    cout<<"Введите номер команды для добавления"<<endl;
    cin>>pos;
    cout<<"1-добавить голкипера, 2-добавить нападающего, 3-добавить защитника"<<endl;
    cin >> q;
    Player* temp = NULL;
    string s, s1, s2;
    int t,st,a,b;
    cout << "Введите имя игрока" << endl;
    cin >> s;
    cout << "Введите фамилию игрока" << endl;
    cin >> s1;
    cout << "Введите отчество игрока" << endl;
    cin >> s2;
    cout << "Введите время в игре" << endl;
    cin >> t;
    cout << "Введите штрафное время" << endl;
    cin >> st;
    switch (q)
    {
        case 1:
        {
            cout << "Введите количество ударов по воротам" << endl;
            cin >> a;
            cout << "Введите количество пропущенных" << endl;
            cin >> b;
            temp = new goalkeeper(s, s1, s2, t, st, a, b);
            break;
            
        }
        case 2:
        {
            cout << "Введите количество ударов по воротам" << endl;
            cin >> a;
            cout << "Введите количество результативных" << endl;
            cin >> b;
            temp = new forw(s,  s1,  s2,  t,  st, a,  b);
            break;
        }
        case 3:
        {
            temp = new defender(s, s1, s2, t, st);
            break;
        }
    }

    if (pos>tableSet.size1())
        return 1;
    mvector<Date>::iterator p;
    int i = 0;
    for (auto p = tableSet.begin(); p != tableSet.end(); ++p)
    {
        if (i == pos)
        {
            (*p).tab->add(temp);
            return 0;
        }
        ++i;
    }
    
    return 0;
}

int komandaCont::delTable(int pos)
{
    if (pos>tableSet.size1())
        return 1;
    
    if (tableSet.empty())
        return 2;
    
    mvector<Date>::iterator p;
    int i = 0;
    for (auto p = tableSet.begin(); p != tableSet.end(); ++p)
    {
        if (pos == i++)
        {
            tableSet.erase(p);
            return 0;
        }
    }
    return 0;
}

int komandaCont::delPlayer()
{
    
    if (tableSet.empty())
        return 2;
    
    mvector<Date>::iterator p;
    for (auto p = tableSet.begin(); p != tableSet.end(); ++p)
    {
        try{
            (*p).tab->del();
            return 0;
        }
        catch (const char* e)
        {
            continue;
        }
    }
    
    return 1;
}
void komandaCont::showPlayerInfo(){
    cout<<"Введите фамилию игрока ";
    string a;
    cin>>a;
    mvector<Date>::iterator p;
    for (auto p = tableSet.begin(); p != tableSet.end(); ++p){
        (*p).tab->showPlayerInfo(a);
    }
}

void komandaCont::show()
{
    mvector<Date>::iterator p;
    int i = 1;
    for (auto p = tableSet.begin(); p != tableSet.end(); ++p)
    {
        cout << endl << "Game " << i++ << " info:" << endl<<"Date: ";
        cout<<(*p).day<<"."<<(*p).month<<"."<<(*p).year<<endl<<"Versus: "<<(*p).versus<<endl;
        (*p).tab->show();
    }
}

int komandaCont::getShTime()
{
    if (tableSet.empty())
        return 0;
    
    mvector<Date>::iterator p;
    int res = 0;
    for (p = tableSet.begin(); p != tableSet.end(); ++p)
    {
        //res += (*p).getShTime();
    }
    return res;
}

int komandaCont::getTableNum()
{
    return (int)tableSet.size1();
}