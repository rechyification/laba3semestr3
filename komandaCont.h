//
//  komandaCont.h
//  laba3semestr3
//
//  Created by Николай on 01.12.14.
//  Copyright (c) 2014 Николай. All rights reserved.
// все игры

#ifndef __laba3semestr3__komandaCont__
#define __laba3semestr3__komandaCont__

#include <stdio.h>
#include "Tablitsa.h"
#include "Player.h"
#include "string.h"
#include "myvector.h"
struct Date{
    int day,month,year;
    string versus;
    Tablitsa* tab;
    Date():day(0),month(0),year(0),tab(NULL),versus(""){}
    Date(int d,int m,int y,Tablitsa* a,string vs):day(d),month(m),year(y),tab(a),versus(vs){}
};
class komandaCont
{
private:
    mvector <Date> tableSet;
public:
    /**Удалить игру по дате */
    int deldate();
    /**Найти игру по дате */
    void finddate();
    /**Добавление элемента в таблицу*/
    int addTable();
    /**Добавить игрока */
    int addPlayer();
    /**Удалить игру по номеру 
     @param
     номер игры*/
    int delTable(int);
    /**Удалить игрока по фамилии */
    int delPlayer();
    /**Вывод содержимого таблицы */
    void show() ;
    /**Получить количество игр */
    int getTableNum();
    /**Получить штрафное время игроков за игру */
    int getShTime();
    /**Показать информацию об игроке по фамилии */
    void showPlayerInfo();
    /**Получить дату, противника и состав команды */
    void datavspl();
    /**получить турнирную таблицу */
    void Turnir();
};
#endif /* defined(__laba3semestr3__komandaCont__) */
