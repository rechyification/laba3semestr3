//
//  main.cpp
//  laba3semestr3
//
//  Created by Николай on 11.11.14.
//  Copyright (c) 2014 Николай. All rights reserved.
// probasda

#include <iostream>
#include "komanda.h"
#include "komandaCont.h"
int TeamD(Team*);
int ex(Team*);
int TeamADD(Team*);
int TeamDel(Team*);
int TeamGetInfo(Team*);
int TeamGetShTime(Team*);
int TeamShow(Team*);
int TeamFindDate(Team*);
int TeamDelDate(Team*);
int TeamInfoPlayer(Team*);
int TeamDataVsPl(Team*);
int TeamTurnir(Team*);
Player* zaprossozdaniya(int );
int main()
{
    Team pl("","","","");
    Team *p = &pl;
    char *msgs[] = {"Действие с игрой","Выход" };
    const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
    int i, exit = 0, ch = 0;
    int(*func[NMsgs])(Team*) = { TeamD, ex };
    while (!exit)
    {
        for (i = 0; i<NMsgs; ++i)
            printf("%d %s \n", i, msgs[i]);
        std::cout << "Выберите пункт из меню:";
        std::cin >> ch;
        //ch = inputNum("Выберите пункт из меню:");
        if ((ch>-1) && (ch<NMsgs))
            exit = (*func[ch])(p);
    }
    std::cout << "Это все, до свидания" << std::endl;
    return 0;
}
int TeamD(Team* pp)
{
    char *msg[] = {"Добавить команду/игрока","Удалить команду/игрока","Вывести штрафное время","Показать содержимое таблицы","Показать название команды, ФИО тренера","Вывести элемент по дате игры","Удалить элемент по дате","Вывести информацию об игроке по фамилии","Вывести дату игры, соперника и состав команды","Вывод турнирной таблицы","Выход" };
    const int amount = sizeof(msg) / sizeof(msg[0]);
    int i, exit = 0, ch = 0;
    int(*func[amount])(Team*) = { TeamADD,TeamDel,TeamGetShTime,TeamShow,TeamGetInfo,TeamFindDate,TeamDelDate,TeamInfoPlayer,TeamDataVsPl,TeamTurnir, ex };
    while (!exit)
    {
        for (i = 0; i<amount; ++i)
            printf("%d %s \n", i, msg[i]);
        std::cout << "Выберите пункт из меню:";
        cin>>ch;
        if ((ch>-1) && (ch<amount))
            exit = (*func[ch])(pp);
    }
    //std::cout << "это все, до свидания!" << std::endl;
    return 0;
}
int ex(Team *pp)
{
    return 1;
}
int TeamGetInfo(Team* q){
    cout<<"Название команды: "<<(*q).getnazv()<<endl;
    cout<<"ФИО тренера: "<<(*q).gettrener()<<endl;
    return 1;
}
int TeamFindDate(Team* q){
    (*q).finddate();
    return 1;
}
int TeamDataVsPl(Team* q){
    (*q).datavspl();
    return 1;
}
int TeamDelDate(Team* q){
    (*q).deldate();
    return 1;
}
int TeamInfoPlayer(Team*q){
    (*q).showplayerinfo();
    return 1;
}
int TeamTurnir(Team* q){
    (*q).turnir();
    return 1;
}
int TeamADD(Team* q)
{
    int t;
    cout<<"1-добавить игру, 2-добавить игрока в игру"<<endl;
    cin>> t;
    switch (t)
    {
        case 1:
            if ((*q).showTableNum() == 0)
            {
                q->setNazv();
                q->settrener();
            }
            q->addTable();
            break;
        case 2:
            q->addPlayer();
    }
    return 1;
}
int TeamDel(Team*q)
{
    int t;
    cout<<"1-Удалить игру,2-удалить игрока из игры"<<endl;
    cin>> t;
    switch (t)
    {
        case 1:
            q->delTable();
            break;
        case 2:
            q->delPlayer();
            break;
    }
    return 1;
}
int TeamGetShTime(Team*q)
{
    (*q).showShTime();
    return 1;
}
int TeamShow(Team*q)
{
    (*q).show();
    return 1;
}
