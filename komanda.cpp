//
//  komanda.cpp
//  laba3semestr3
//
//  Created by Николай on 01.12.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//

#include "komanda.h"
#include <iostream>

Team::Team(string naz,string n,string f,string o) :trener(n,f,o),nazv(naz){};
string Team::getnazv(){
    return nazv;
}
void Team::setNazv(){
    cout<<"Введите название комманды: ";
    cin>>nazv;
}
string Team::gettrener(){
    return (trener.getSurname()+" "+trener.getName()+" "+trener.getPatronymic());
}
void Team::datavspl(){
    kom.datavspl();
}
void Team::settrener(){
    trener.setCapitan();
}
void Team::turnir(){
    kom.Turnir();
}
int inputNum(const char* s)
{
    int num;
    do{
        cout << s << endl;
        cin.clear();
        cin.sync();
        cin >> num;
    } while (cin.fail() || num<1);
    return num;
}
void Team::addTable()
{
    try {
                kom.addTable();
    }
    catch (const char* e)
    {
        cout << e << endl;
    }

}


void Team::addPlayer()
{
    try{
        kom.addPlayer();
    }
    catch (const char* e)
    {
        cout << e << endl;
    }
}

void Team::delTable()
{
    int pos = inputNum("Введите номер игры: ");
    
    try{
        kom.delTable(pos);
    }
    catch (const char* e)
    {
        cout << e << endl;
    }
}
void Team::showplayerinfo(){
    kom.showPlayerInfo();
}
void Team::delPlayer()
{

    try{
        kom.delPlayer();
    }
    catch (const char* e)
    {
        cout << e << endl;
    }
}
void Team::finddate(){
    kom.finddate();
}
int Team::deldate(){
    return (kom.deldate());
}
void Team::show()
{
    kom.show();
    
}

int Team::showTableNum()
{
    return kom.getTableNum();
}

void Team::showShTime()
{
    cout << kom.getShTime() << endl;
}


