//
//  komanda.h
//  laba3semestr3
//
//  Created by Николай on 11.11.14.
//  Copyright (c) 2014 Николай. All rights reserved.
// одна игра

#ifndef __laba3semestr3__komanda__
#define __laba3semestr3__komanda__

#include <stdio.h>
#include "Tablitsa.h"
#include "Player.h"
#include "komandaCont.h"
using namespace std;
#include<string>
class Team
{/** class Team */
private:
    komandaCont kom;/**<Задает имя тренера*/
    Capitan trener;/**<Задает название команды */
    string nazv;
public:
    Team(string,string,string,string);
    Team():nazv(0),trener(NULL,NULL,NULL),kom(){}
    Team(const Team &T){
        nazv=T.nazv;
        kom = T.kom;
        trener = T.trener;
    }
    /**
     Добавление элемента в таблицу
     */
    void addTable();/**Установить название команды */
    void setNazv();/**Установить ФИО тренера */
    void settrener();/**Получить название команды*/
    string getnazv();/**Получить ФИО тренера */
    string gettrener();/**Найти игру по дате */
    void finddate();/**Удалить игру по дате */
    int deldate();/**Показать информацию об игроке по фамилии */
    void showplayerinfo(); /**Добавить игрока */
    void addPlayer();/**Удалить игру по номеру */
    void delTable();/**Удалить игрока по фамилии */
    void delPlayer();/**Вывод содержимого таблицы */
    void show();/**Показать количество игр */
    int showTableNum() ;/**Получить штрафное время игроков за игру */
    void showShTime();/**Получить дату, противника и состав команды */
    void datavspl();/**получить турнирную таблицу */
    void turnir();/**Деструктор */
    virtual~Team(){}
};
#endif /* defined(__laba3semestr3__komanda__) */
