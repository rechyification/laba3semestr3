//
//  Player.h
//  laba3semestr3
//
//  Created by Николай on 11.11.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//

#ifndef __laba3semestr3__Player__
#define __laba3semestr3__Player__

#include <stdio.h>
#pragma once
using namespace std;
#include <string>
class Capitan
{
private:
    string firstname;
    string surname;
    string patronymic;
public:
    Capitan():firstname(nullptr),surname(nullptr),patronymic(nullptr){}
    Capitan(string n, string n1, string n2) :firstname(n), surname(n1), patronymic(n2){}
    string getName() ;
    string getSurname() ;
    string getPatronymic() ;
    void setCapitan();
    virtual~Capitan(){}
};
class Player
{
private:
    Capitan name;
    int time_v_igre;
    int shtrafnoe_time;
public:
    /**Конструктор 
     @param имя
     @param фамилия
     @param отчество
     @param время в игре
     @param штрафное время*/
    Player(string n, string n1, string n2, int time, int stime) : name(n, n1, n2), time_v_igre(time), shtrafnoe_time(stime){}
    Player():time_v_igre(0),shtrafnoe_time(0),name("","",""){}
    /**Получить фамилию
     @return фамилия*/
    virtual string getF();
    /**Получить ФИО игрока
     @return ФИО*/
    string getPlayerInfo();
    /**Получить амплуа
     @return амплуа*/
    virtual string getamplua()  {return ("defender");};
    /**Получить время в игре
     @return время*/
    int getTime_v_igre();
    /**Получить штрафное время в игре
     @return время*/
    int getShtrafnoe_time();
    /**Установить время в игре
     @param время*/
    void settime_v_igre(int a);
    /**Установить штрафное время в игре
     @param время*/
    void setshtrafnoe_time(int a);
    /**Вывод таблицы 
     @param поток*/
    virtual ostream& show(ostream&) ;
    /**Вывод игрока 
     @param поток*/
    friend ostream& operator <<(std::ostream& stream, Player&);
    /**Деструктор */
    virtual~Player(){};
};

class goalkeeper:public Player{
private:
    int kolvoBroskov;
    int kolvoRezult = 0;
public:
    /**Конструктор
     @param имя
     @param фамилия
     @param отчество
     @param время в игре
     @param штрафное время
     @param количество бросков
     @param количество результативных*/
    goalkeeper(string n, string n1, string n2, int time, int stime,int k, int kk);
    /** Копирующий конструктор*/
    goalkeeper( goalkeeper &gk){
        kolvoBroskov=gk.kolvoBroskov;
        kolvoRezult=gk.kolvoRezult;
        setshtrafnoe_time(gk.getShtrafnoe_time());
        settime_v_igre(gk.getTime_v_igre());
        //setCapitan(gk.getName(), gk.getSurname(), gk.getPatronymic());
    }
    /** Перемещающий конструктор*/
    goalkeeper(goalkeeper&& x): kolvoBroskov(x.kolvoBroskov), kolvoRezult(x.kolvoRezult){
        x.kolvoBroskov = 0;
        x.kolvoRezult = 0;
    }
    /**Получить амплуа
     @return амплуа*/
    string getamplua(){return ("goalkeeper");};
    /**Получить количество бросков
     @return кол-во*/
    int getKolvoBroskov(){return kolvoBroskov;};
    /**Получить количество результативных бросков
     @return кол-во*/
    int getKolvoRezult(){return kolvoRezult;};
    /**Вывод игрока
     @param поток*/
    friend ostream& operator <<(std::ostream& stream, goalkeeper&);
    /**Вывод игрока
     @param поток*/
    virtual ostream& show(ostream&) ;
    
};
class forw:public Player{
private:
    int kolvoBroskov1;
    int kolvoRezult1 = 0;
public:
    /**Конструктор
     @param имя
     @param фамилия
     @param отчество
     @param время в игре
     @param штрафное время
     @param количество бросков
     @param количество результативных*/
    forw(string n, string n1, string n2, int time, int stime,int k, int kk);
    /** Копирующий конструктор*/
    forw( forw &fw){
        kolvoBroskov1=fw.kolvoBroskov1;
        kolvoRezult1=fw.kolvoRezult1;
        setshtrafnoe_time(fw.getShtrafnoe_time());
        settime_v_igre(fw.getTime_v_igre());
        //setCapitan(fw.getName(), fw.getSurname(), fw.getPatronymic());
    }
    /** Перемещающий конструктор*/
    forw(forw&& x): kolvoBroskov1(x.kolvoBroskov1), kolvoRezult1(x.kolvoRezult1){
        x.kolvoBroskov1 = 0;
        x.kolvoRezult1 = 0;
    }
    /**Получить амплуа
     @return амплуа*/
    string getamplua(){return ("forward");};
    /**Получить количество бросков
     @return кол-во*/
    int getKolvoBroskov(){return kolvoBroskov1;};
    /**Получить количество результативных бросков
     @return кол-во*/
    int getKolvoRezult1(){return kolvoRezult1;};
    /**Вывод игрока
     @param поток*/
    friend ostream& operator <<(std::ostream& stream, forw&);
    /**Вывод игрока
     @param поток*/
    virtual ostream& show(ostream&) ;
};
class defender:public Player{
public:
    /**Конструктор
     @param имя
     @param фамилия
     @param отчество
     @param время в игре
     @param штрафное время*/
    defender(string n, string n1, string n2, int time, int stime) :Player(n,n1,n2,time,stime){}
    /** Копирующий конструктор*/
    defender( defender &df){
        setshtrafnoe_time(df.getShtrafnoe_time());
        settime_v_igre(df.getTime_v_igre());
        //setCapitan(df.getName(), df.getSurname(), df.getPatronymic());
    }
    /**Вывод игрока
     @param поток*/
    friend ostream& operator <<(std::ostream& stream, defender&);
    //virtual ostream& show(ostream&) const;
};
#endif /* defined(__laba3semestr3__Player__) */
