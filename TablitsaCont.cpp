//
//  TablitsaCont.cpp
//  laba3semestr3
//
//  Created by Николай on 01.12.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//

#include "TablitsaCont.h"
#include "iostream"
using namespace std;
int TablitsaCont::add(Player* x)
{
    Unit* temp = new Unit(x);
    units.push_back(*temp);
    return 0;
}
void TablitsaCont::getplinfo(){
    mvector<Unit>::iterator p;
    for (auto p = units.begin(); p != units.end(); ++p){
        cout<<(*p).player->getPlayerInfo()<<endl;
    }
    
}
int TablitsaCont::del()
{
    cout << "Введите фамилию игрока, которого хотите удалить: " << endl;
    string a;
    cin >> a;
    mvector<Unit>::iterator p;
    for (auto p = units.begin(); p != units.end(); ++p)
    {
        if ((*p).player->getF() == a)
        {
            units.erase(p);
            return 0;
        }
    }
    return 1;
}

void TablitsaCont::showPlayerInfo(string a)
{
        for (auto p = units.begin(); p != units.end(); ++p)
        {
            if ((*p).player->getF() == a)
            {
                cout << (*((*p).player)) << endl;
                return;
            }
        }
}

int TablitsaCont::getShTime()
{   int a = 0;
    mvector<Unit>::iterator p;
    for (auto p = units.begin(); p != units.end(); ++p)
    {
        a += (*p).player->getShtrafnoe_time();
    }
    return a;
}
int TablitsaCont::Turnir1_propush(){
    goalkeeper *t = nullptr;
    //forw *tt = nullptr;
    string ampl;
    int p = 0;
    mvector<Unit>::iterator pp;
    for (auto pp = units.begin(); pp != units.end(); ++pp)
    {
        ampl = (*pp).player->getamplua();
        if (ampl == "goalkeeper"){
            t = dynamic_cast<goalkeeper*>((*pp).player);
            p +=t->getKolvoRezult();
        }
}
    return p;
}
int TablitsaCont::Turnir2_zabit(){
    //goalkeeper *t = nullptr;
    forw *tt = nullptr;
    string ampl;
    int p = 0;
    mvector<Unit>::iterator pp;
    for (auto pp = units.begin(); pp != units.end(); ++pp)
    {
        ampl = (*pp).player->getamplua();
        if (ampl == "forward"){
            tt = dynamic_cast<forw*>((*pp).player);
            p +=tt->getKolvoRezult1();
        }
    }
    return p;
    
}
void TablitsaCont::clearTabl()
{
    mvector<Unit>::iterator p;
    for (auto p = units.begin(); p != units.end(); ++p)
    {
        delete (*p).player;
        units.erase(p);
    }
}

ostream &operator<<(ostream& out, TablitsaCont& x)
{
    
    mvector<Unit>::iterator p;
    
    if (x.units.empty())
        out << "В команде нет игроков!";
    int i = 1;
    for (auto p = x.units.begin(); p != x.units.end(); ++p)
    {
        out << endl << "Игрок  # " << i++ << " Инфо:" << endl;
        out << endl << (*((*p).player)) << endl << endl;
    }
    return out;
}
