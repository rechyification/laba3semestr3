//
//  Tablitsa.h
//  laba3semestr3
//
//  Created by Николай on 11.11.14.
//  Copyright (c) 2014 Николай. All rights reserved.
//

#ifndef __laba3semestr3__Tablitsa__
#define __laba3semestr3__Tablitsa__
#include <stdio.h>
#include "myvector.h"
#include <string>
#include "Player.h"
#include "TablitsaCont.h"
class Tablitsa
{
private:
    TablitsaCont table;
public:
    /**Добавить игрока 
     @param указатель на игрока*/
    int add(Player*);
    /**Удалить игрока по фамилии */
    int del();
    /**Вывод содержимого таблицы */
    void show();
    /**Показать информацию об игроке по фамилии
     @param фамилия*/
    void showPlayerInfo(string a);
    /**Получить дату, противника и состав команды */
    void getplinfo();
    /**Получить штрафное время игроков за игру */
    int getShTime() ;
    /**получить количество пропущенных мячей
     @return кол-во мячей*/
    int Turnir1();
    /**получить количество забитых мячей
     @return кол-во мячей*/
    int Turnir2();
     /**Очистить таблицу */
    void clearTable();
    /**Деструктор */
    virtual~Tablitsa(){};
};
#endif /* defined(__laba3semestr3__Tablitsa__) */
